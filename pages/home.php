
<body>
  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Blog Entries Column -->
      <div class="col-md-8">

        <h1 class="my-4 titre">Overwatch-E-sport actu :
          <small></small>
        </h1>

        <!-- Blog Post -->
        <div class="card mb-4">
          <img class="card-img-top" src="./images/owl.jpeg" alt="Card image cap">
          <div class="card-body">
            <h2 class="card-title">Post Title</h2>
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
          </div>
          <div class="card-footer text-muted">
            Posté le 1er janvier par Yohan
          </div>
        </div>

        <!-- Blog Post -->
        <div class="card mb-4">
          <img class="card-img-top" src="./images/owlmatch.png" alt="Card image cap">
          <div class="card-body">
            <h2 class="card-title">Post Title</h2>
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
          </div>
          <div class="card-footer text-muted">
            Posté le 1er janvier par Yohan
          </div>
        </div>

        <!-- Blog Post -->
        <div class="card mb-4">
          <img class="card-img-top" src="./images/mcree.jpg" alt="Card image cap">
          <div class="card-body">
            <h2 class="card-title">Post Title</h2>
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
          </div>
          <div class="card-footer text-muted">
            Posté le 1er janvier par Yohan
          </div>
        </div>

      </div>
      
      <!-- Sidebar Widgets Column -->
      
      <div class="col-md-3 fixe position-fixed">

        <!-- Search Widget -->
        <div class="card my-4 padding bg-light">
          <h5 class="card-header">Prochain Match</h5>
          <div class="card-body">
            <div class="input-group">
              <p>Paris Eternal VS L.A Valiant 15/12/xxxx</p>
              <p>Houston Outlaw VS SF Shock 15/12/xxxx</p>
            </div>
          </div>
        </div>

        <!-- Categories Widget -->
        <div class="card my-4 bg-light">
          <h5 class="card-header">Dernier patch</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="https://playoverwatch.com/en-us/news/patch-notes/live">29/10/2020</a>
                  </li>
                  <li>
                    <a href="https://playoverwatch.com/en-us/news/patch-notes/live/2020/09">29/09/2020</a>
                  </li>
                  <li>
                    <a href="https://playoverwatch.com/en-us/news/patch-notes/live/2020/08">31/08/2020</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="https://playoverwatch.com/en-us/news/patch-notes/live/2020/07">30/07/2020</a>
                  </li>
                  <li>
                    <a href="https://playoverwatch.com/en-us/news/patch-notes/live/2020/06">30/08/2020</a>
                  </li>
                  <li>
                    <a href="https://playoverwatch.com/en-us/news/patch-notes/live/2020/05">20/07/2020</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- Side Widget -->
       <div class="card my-4 bg-light">
          <h5 class="card-header">Derniers resultats</h5>
          <div class="card-body">
            <div class="input-group">
              <p><span style="color:green">PhiladelphiaFusion</span>  VS <span style="color:red">Seoul Dynasty</span> 3-0</p>
              <p><span style="color:red">Shangai Dragons</span> VS <span style="color:green">London Spitfire</span> 2-3</p>
            </div>
          </div>
        </div>

      </div>

    </div>
    <!-- /.row -->

  </div>
</body>
